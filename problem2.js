const fsp = require("fs/promises")

const path = require("path");

function lipsumFile() {
    const myPromise = fsp.readFile(path.join(__dirname, "./lipsum.txt"), "utf-8")


    myPromise.then((content) => {
        console.log("lipsum.txt file Reading is Completed");
        return fsp.writeFile(path.join("./upperCase.txt"), content.toUpperCase());
    }).then(() => {
        console.log("UpperCase.txt file is being created");
        return fsp.writeFile(path.join("./filenames.txt"), "upperCase.txt \n");
    }).then(() => {
        console.log("upperCase.txt file name is appended to filenames.txt..........");
        return fsp.readFile(path.join("upperCase.txt"), "utf-8");
    }).then((content) => {
        console.log("converting upperCase.txt file  to lower case and split now");
        let split = content.toLowerCase().split(".");
        return fsp.writeFile(path.join("lowerCase.txt"), JSON.stringify(split));
    }).then(() => {
        console.log("LowerCase.txt is added to filenames.txt.......... ");
        return fsp.appendFile(path.join("filenames.txt"), "LowerCase.txt \n");
    }).then(() => {
        console.log("sorting started .............");
        return fsp.readFile(path.join("lowerCase.txt"), "utf-8");
    }).then((content) => {
        console.log("sorted.txt file is ready....");
        let dataSorted = JSON.parse(content).sort();
        return fsp.writeFile(path.join("sorted.txt"), JSON.stringify(dataSorted));
    }).then(() => {
        console.log("sorted.txt is added to filenames.txt.......");
        return fsp.appendFile(path.join("filenames.txt"), "sorted.txt \n");
    }).then(() => {
        return fsp.readFile(path.join(__dirname, './filenames.txt'), 'utf-8')
    }).then(() => {
        console.log("sorted.txt is appended to filenames.txt");
        return fsp.unlink(path.join("upperCase.txt"));
    }).then(() => {
        console.log("upperCase.txt is deleted");
        return fsp.unlink(path.join("lowerCase.txt"));
    }).then(() => {
        console.log("lowerCase.txt is deleted");
        return fsp.unlink(path.join("sorted.txt"));
    }).then(() => {
        console.log("sorted.txt is deleted");
    }).catch((err)=>{
        console.error(err)
    })


}
module.exports = lipsumFile;