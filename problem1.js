function directoryAndFile(randomNumber) {
    const fsp = require("fs/promises");
    const path = require('path');

    const myPromise = fsp.mkdir("checkFolder")
    let randomNumberArray = [];
    for (let count = 1; count < randomNumber; count++) {
        randomNumberArray.push(count);
    }

    myPromise.then(() => {
        console.log("Directory created");
        let fileWriteArray = [];
        fileWriteArray.length = randomNumber;
        let fileWriteArrayPromise = randomNumberArray.map(file => fsp.writeFile(path.join(__dirname, `checkFolder/file${file}.json`), "My file content"));
        return Promise.all(fileWriteArrayPromise);

    }).then(() => {
        console.log("All files created");
        let filesDeleteArrayPromise = randomNumberArray.map(file => fsp.unlink(path.join(__dirname,`checkFolder/file${file}.json`)));
        return Promise.all(filesDeleteArrayPromise);
    }).then(() => {
        console.log("all files deleted");
    }).catch((err) => {
        console.error(err);
    })
}

module.exports = directoryAndFile;
